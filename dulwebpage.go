package main

import (
	"io/ioutil"
)

type DULWebPage struct {
	Title string
	Body []byte
}

func (p *DULWebPage) Write(buf []byte) (int, error) {
	p.Body = append(p.Body, buf...)
	return 0, nil
}

func loadPage(title string) (*DULWebPage, error) {
	filename := title + ".txt"
	body, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return &DULWebPage{Title: title, Body: body}, nil
}
