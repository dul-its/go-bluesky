package main

import (
	"net/http"
)

func Welcome(w http.ResponseWriter, r *http.Request) {
	Info.Printf("serving %s", r.URL.Path)
	
	// This is the "context" data, expected by the "masterTemplate"
	page := DULWebPage{Title: "Welcome, and Go!", Body: []byte("Hello There.  How are you?")}
	
	// The masterTemplate has placeholders for 'Body' and 'Title'
	masterTemplate.Execute(w, page)
}
