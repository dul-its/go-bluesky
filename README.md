# go-bluesky

## What Is This?
The idea behind this project is to (hopefully) demonstrate how `golang` (Go) can be used for application development.  
  
For this "blue sky" project, I will create a simple web server that responds to a limited subset of routes.
  
### Prerequisites
* [How to Write Go Code](https://golang.org/doc/code.html)
* [HTTP Request Multiplexer](http://www.gorillatoolkit.org/pkg/mux)
