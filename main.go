package main

import (
	_ "fmt"
	_ "gopkg.in/ini.v1"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
	"os"	
	"text/template"
	"time"
	"log"
	_ "gitlab.oit.duke.edu/go-bluesky/handlers"
	"flag"
)

// load and parse the MASTER DUL template here
// (so as to be efficient and not call this for every handler)
// and have the template's "Execute" write into 'w'
// prior to deferring to the app-specific handler
// TODO: move to separate file
var masterTemplate = template.Must(template.ParseFiles("templates/master/dul_master.html"))

func middlewareGzipCompress(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			Info.Println("attempting to enable compression for asset...")
			w.Header().Set("Content-Encoding", "gzip")
			next.ServeHTTP(w, r)
		})
}

func main() {
	// directory from which to serve files from
	// (should be configurable)
	var assetsDir string
	
	flag.StringVar(&assetsDir, "assets-dir", "assets", "the directory to serve asset files from .")
	flag.Parse()
	
	// InitLogging intializes "Trace", "Info", "Warning" and "Error" respectively
	InitLogging(ioutil.Discard, os.Stdout, os.Stdout, os.Stderr)
	
	routes := mux.NewRouter()
	
	assetsRoute := routes.PathPrefix("/assets/").Subrouter()
	assetsRoute.Use(middlewareGzipCompress)
	//assetRoute.Handler(http.StripPrefix("/assets/", http.FileServer(http.Dir(assetsDir))))
	//assetRoute.Use(middlewareGzipCompress)
	
	routes.PathPrefix("/assets/").
		Handler(http.StripPrefix("/assets/", http.FileServer(http.Dir(assetsDir))))
		
	routes.HandleFunc("/", Welcome)

	// create the server
	srv := &http.Server{
		Handler:		routes,
		Addr:			"127.0.0.1:8080",
		// Good practice: enforce timeouts for servers
		WriteTimeout:	15 * time.Second,
		ReadTimeout:	15 * time.Second,
	}
	
	Info.Println("Listening and serving on port 8080")
	
	log.Fatal(srv.ListenAndServe())
}